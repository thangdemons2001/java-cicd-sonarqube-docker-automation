package com.example.demo.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;


import com.example.demo.entity.Templates;


@RestController
@RequestMapping(value = "/api/v1/zalo/business/openapi")
@CrossOrigin("*")
public class ZnsController {

	@Autowired
	private RestTemplate restTemplate;
	
	@GetMapping("/")  
	public String hello()   
	{  
		return "Hello world!";  
	}  

	@GetMapping("/template/all")
	public ResponseEntity<?> getListZnsTemplate(int offset, int limit,
			@RequestHeader("x_access_token") String accessToken) {
		
		System.out.println(offset);
		
		System.out.println(limit);
		
		Map<String, Integer> params = new HashMap<String, Integer>();
		params.put("offset", offset);
		params.put("limit", limit);

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.set("access_token", accessToken);

		HttpEntity<String> entity = new HttpEntity<>(headers);

		ResponseEntity<Templates> templates = restTemplate.exchange("https://business.openapi.zalo.me/template/all?offset={offset}&limit={limit}",
				HttpMethod.GET, entity, Templates.class, params);

		System.out.println(templates);

		return new ResponseEntity<ResponseEntity<Templates>>(templates, HttpStatus.OK);
	}
	
	@GetMapping("/template/info")
	public ResponseEntity<?> getDetailTemplate(long template_id,
			@RequestHeader("x_access_token") String accessToken) {
		
		
		Map<String, Long> params = new HashMap<String, Long>();
		params.put("template_id", template_id);

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.set("access_token", accessToken);

		HttpEntity<String> entity = new HttpEntity<>(headers);

		ResponseEntity<Object> res = restTemplate.exchange("https://business.openapi.zalo.me/template/info?template_id={template_id}",
				HttpMethod.GET, entity, Object.class, params);
		

		return new ResponseEntity<ResponseEntity<Object>>(res, HttpStatus.OK);
	}
	
	
	@PostMapping("/message/template")
	public ResponseEntity<?> sendMessageToClientDevelopmentMode(@RequestBody Object object,
			@RequestHeader("x_access_token") String accessToken) {
		
		

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.set("access_token", accessToken);

		HttpEntity<Object> entity = new HttpEntity<>(object,headers);

		ResponseEntity<Object> res = restTemplate.exchange("https://business.openapi.zalo.me/message/template",
				HttpMethod.POST, entity, Object.class);
		

		return new ResponseEntity<ResponseEntity<Object>>(res, HttpStatus.OK);
	}
	
	

}
